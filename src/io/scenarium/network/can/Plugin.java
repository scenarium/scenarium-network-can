package io.scenarium.network.can;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.network.can.communication.can.dbc.EncodingMessageProperties;
import io.scenarium.network.can.communication.can.dbc.MessageIdentifier;
import io.scenarium.network.can.communication.can.dbc.SignalIdentifier;
import io.scenarium.network.can.editors.can.CanTrameEditor;
import io.scenarium.network.can.editors.can.dbc.EncodingMessagePropertiesEditor;
import io.scenarium.network.can.editors.can.dbc.MessageIdentifierEditor;
import io.scenarium.network.can.editors.can.dbc.SignalIdentifierEditor;
import io.scenarium.network.can.model.CanTrame;
import io.scenarium.network.can.operator.network.bus.can.CAN;
import io.scenarium.network.can.operator.network.decoder.CanDBCDecoder;
import io.scenarium.network.can.operator.network.encoder.CanDBCEncoder;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(CAN.class);
		operatorConsumer.accept(CanDBCDecoder.class);
		operatorConsumer.accept(CanDBCEncoder.class);
	}

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(CanTrame.class, CanTrame::clone);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("dataStructure.can.CanTrame", "io.scenarium.network.can.communication.can.CanTrame");
		classNameRedirectionConsumer.accept("io.scenarium.core.communication.can.CanTrame", "io.scenarium.network.can.communication.can.CanTrame");
		classNameRedirectionConsumer.accept("io.scenarium.core.communication.can.dbc.EncodingMessageProperties", "io.scenarium.network.can.communication.can.dbc.EncodingMessageProperties");
		classNameRedirectionConsumer.accept("io.scenarium.core.communication.can.dbc.MessageIdentifier", "io.scenarium.network.can.communication.can.dbc.MessageIdentifier");
		classNameRedirectionConsumer.accept("io.scenarium.core.communication.can.dbc.SignalIdentifier", "io.scenarium.network.can.communication.can.dbc.SignalIdentifier");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.bus.can.CAN", "io.scenarium.network.can.operator.network.bus.can.CAN");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.decoder.CanDBCDecoder", "io.scenarium.network.can.operator.network.decoder.CanDBCDecoder");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.encoder.CanDBCEncoder", "io.scenarium.network.can.operator.network.encoder.CanDBCEncoder");
	}

	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(CanTrame.class, CanTrameEditor.class);
		editorConsumer.accept(SignalIdentifier.class, SignalIdentifierEditor.class);
		editorConsumer.accept(MessageIdentifier.class, MessageIdentifierEditor.class);
		editorConsumer.accept(EncodingMessageProperties[].class, EncodingMessagePropertiesEditor.class);

	}
}
