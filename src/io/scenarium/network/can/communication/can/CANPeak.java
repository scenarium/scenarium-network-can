/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.communication.can;

import java.util.ArrayList;

import io.beanmanager.editors.DynamicChoiceBox;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.core.tools.LibraryUtils;
import io.scenarium.network.can.internal.Log;
import io.scenarium.network.can.model.CanTrame;

import peak.can.basic.PCANBasic;
import peak.can.basic.TPCANBaudrate;
import peak.can.basic.TPCANHandle;
import peak.can.basic.TPCANMessageType;
import peak.can.basic.TPCANMode;
import peak.can.basic.TPCANMsg;
import peak.can.basic.TPCANStatus;
import peak.can.basic.TPCANTimestamp;
import peak.can.basic.TPCANType;

public class CANPeak extends CanProvider implements DynamicEnableBean {
	private static boolean isAvailable = LibraryUtils.loadLibrariesFromRessources(CANPeak.class, e -> "\nDrivers of the Peak's card probably not installed", "PCANBasic", "PCANBasic_JNI");

	public static void sample() {
		PCANBasic can = null;
		TPCANMsg msg = null;
		TPCANStatus status = null;
		can = new PCANBasic();
		if (!can.initializeAPI()) {
			Log.info("Unable to initialize the API");
			System.exit(0);
		}
		status = can.Initialize(TPCANHandle.PCAN_PCIBUS1, TPCANBaudrate.PCAN_BAUD_1M, TPCANType.PCAN_TYPE_NONE, 0, (short) 0);
		msg = new TPCANMsg();
		while (true)
			while (can.Read(TPCANHandle.PCAN_PCIBUS1, msg, null) == TPCANStatus.PCAN_ERROR_OK) {
				status = can.Write(TPCANHandle.PCAN_PCIBUS1, msg);
				if (status != TPCANStatus.PCAN_ERROR_OK) {
					Log.info("Unable to write the CAN message");
					System.exit(0);
				}
			}

	}

	private PCANBasic can;
	@PropertyInfo(index = 0)
	@DynamicChoiceBox(possibleChoicesMethod = "getAvailableChannel", useSwingWorker = true)
	private TPCANHandle channel;
	@PropertyInfo(index = 1)
	private TPCANBaudrate baudRate = TPCANBaudrate.PCAN_BAUD_500K;
	@PropertyInfo(index = 2)
	private FilterType filterType = FilterType.STANDARD;
	@PropertyInfo(index = 3)
	@NumberInfo(min = 0, max = CanTrame.ADRESSE_MAX_CAN_EXT)
	private int from = 0;
	@PropertyInfo(index = 4)
	@NumberInfo(min = 0, max = CanTrame.ADRESSE_MAX_CAN_EXT)
	private int to = CanTrame.ADRESSE_MAX_CAN_EXT;
	@PropertyInfo(index = 5)
	private TPCANType hardwareType = TPCANType.PCAN_TYPE_NONE;
	@PropertyInfo(index = 6)
	private int ioport = 100;

	@PropertyInfo(index = 7)
	private short interrupt = 3;
	private boolean isInitialize = false;
	// Variable tampon
	private final TPCANMsg sendMsgBuff = new TPCANMsg();

	private final TPCANMsg rcvMsgBuff = new TPCANMsg();

	private final TPCANTimestamp rcvtTsBuff = new TPCANTimestamp();

	@Override
	public void connect() throws CanException {
		if (!isAvailable)
			throw new CanException("Can provider not available");

		CanPeakManager cpm = CanPeakManager.getInstance();
		if (this.can == null) {
			this.can = cpm.getPCANBasic();
			if (!this.can.initializeAPI()) {
				this.can = null;
				throw new CanException("Cannot initialize API");
			}
		}

		if (this.channel == null)
			throw new CanException("No channel selected");
		String error = (isNonPnP() ? this.can.Initialize(this.channel, this.baudRate, this.hardwareType, this.ioport, this.interrupt)
				: this.can.Initialize(this.channel, this.baudRate, TPCANType.PCAN_TYPE_NONE, 0, (short) 0)).getMessage();
		if (error != null)
			throw new CanException("Cannot init channel: " + this.channel + ", error code: " + error);
		this.isInitialize = true;
		if ((this.from != 0 || this.to != CanTrame.ADRESSE_MAX_CAN_EXT) && this.to > this.from) {
			error = this.can.FilterMessages(this.channel, this.from, this.to, this.filterType == FilterType.STANDARD ? TPCANMode.PCAN_MODE_STANDARD : TPCANMode.PCAN_MODE_EXTENDED).getMessage();
			if (error != null)
				throw new CanException("Cannot set filtering, error code: " + error);
		}
		error = this.can.SetRcvEvent(this.channel).getMessage();
		if (error != null) {
			Log.error("Cannot set the RcvEvent, error code: " + error);
			return;
		}
		cpm.setrcvEventDispatcher(this.channel, this);
	}

	@Override
	public TPCANHandle[] getAvailableChannel() {
		if (!isAvailable)
			return new TPCANHandle[0];
		if (this.can == null) {
			try {
				this.can = CanPeakManager.getInstance().getPCANBasic();
			} catch (CanException e) {
				return new TPCANHandle[0];
			}
			if (!this.can.initializeAPI()) {
				this.can = null;
				return new TPCANHandle[0];
			}
		}
		ArrayList<TPCANHandle> availableChannel = new ArrayList<>();
		for (TPCANHandle canHandle : TPCANHandle.initializableChannels())
			// StringBuffer sb = new StringBuffer();
			// can.GetValue(canHandle, TPCANParameter.PCAN_CHANNEL_CONDITION, sb, 1);
			// if ((canHandle == channel && can != null) || (sb.length() != 0 && ((int) sb.charAt(0)) == TPCANParameterValue.PCAN_CHANNEL_AVAILABLE.getValue())) {
			availableChannel.add(canHandle);
		// String error = (isNonPnP() ? can.Initialize(canHandle, baudRate, hardwareType, ioport, interrupt) : can.Initialize(channel, baudRate, TPCANType.PCAN_TYPE_NONE, 0, (short) 0)).getMessage();
		// if (error != null){
		// can.Uninitialize(canHandle)
		// }
		// }
		// sb.setLength(0);
		// if (this.can == null) {
		// String error = can.Uninitialize(TPCANHandle.PCAN_NONEBUS).getMessage();
		// if (error != null)
		// Log.error(error);
		// }
		this.can = null;
		return availableChannel.toArray(TPCANHandle[]::new);
	}

	public TPCANBaudrate getBaudRate() {
		return this.baudRate;
	}

	public TPCANHandle getChannel() {
		return this.channel;
	}

	public FilterType getFilterType() {
		return this.filterType;
	}

	public int getFrom() {
		return this.from;
	}

	public TPCANType getHardwareType() {
		return this.hardwareType;
	}

	public short getInterrupt() {
		return this.interrupt;
	}

	public int getIoport() {
		return this.ioport;
	}

	public int getTo() {
		return this.to;
	}

	private boolean isNonPnP() {
		return this.channel == null ? false : TPCANHandle.isPCANUSBHardware(this.channel) || this.channel == TPCANHandle.PCAN_DNGBUS1;
	}

	public void processRcvEvent(TPCANHandle canChannel) {
		while (true) {
			TPCANStatus status = this.can.Read(canChannel, this.rcvMsgBuff, this.rcvtTsBuff);
			if (status != TPCANStatus.PCAN_ERROR_OK) {
				if (status != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
					Log.error("Error while reading trame, error code: " + status.getMessage());
				return;
			}
			if (this.rcvMsgBuff.getType() == TPCANMessageType.PCAN_MESSAGE_EXTENDED.getValue())
				this.receiver.receive(new CanTrame(this.rcvMsgBuff.getID(), true, this.rcvMsgBuff.getData()), System.currentTimeMillis()/* ts.getMillis() */);
			else if (this.rcvMsgBuff.getType() == TPCANMessageType.PCAN_MESSAGE_STANDARD.getValue())
				this.receiver.receive(new CanTrame(this.rcvMsgBuff.getID(), false, this.rcvMsgBuff.getData()), System.currentTimeMillis()/* ts.getMillis() */);
		}
	}

	@Override
	public void release() {
		if (this.can == null || !this.isInitialize)
			return;
		if (this.can.GetStatus(this.channel) != TPCANStatus.PCAN_ERROR_INITIALIZE) {
			String error = this.can.ResetRcvEvent(this.channel).getMessage();
			if (error != null)
				Log.error(error);
			error = this.can.Uninitialize(this.channel).getMessage();
			if (error != null)
				Log.error("Cannot close the driver, error code: " + error);
		}
		this.can = null;
	}

	@Override
	public void sendTrame(CanTrame canTrame) {
		if (this.filterType == FilterType.STANDARD && canTrame.isExtendedFrame()) {
			Log.error("cannot send trame: " + canTrame + "\nId larger than 11 bits specified in standard can 2.0A");
			return;
		}
		byte[] datas = canTrame.getData();
		this.sendMsgBuff.setID(canTrame.getId());
		this.sendMsgBuff.setType(this.filterType == FilterType.STANDARD ? TPCANMessageType.PCAN_MESSAGE_STANDARD : TPCANMessageType.PCAN_MESSAGE_EXTENDED);
		this.sendMsgBuff.setLength((byte) datas.length);
		this.sendMsgBuff.setData(datas, (byte) datas.length);
		this.can.Write(this.channel, this.sendMsgBuff).getMessage();
	}

	public void setBaudRate(TPCANBaudrate baudRate) {
		this.baudRate = baudRate;
		restart();
	}

	public void setChannel(TPCANHandle channel) {
		boolean needToRestart = this.can != null;
		CANReceiver oldReceiver = null;
		if (needToRestart) {
			oldReceiver = this.receiver;
			close();
		}
		this.channel = channel;
		setEnable();
		if (needToRestart)
			try {
				init(oldReceiver);
			} catch (CanException e) {
				Log.error(e.getMessage());
			}
	}

	@Override
	public void setEnable() {
		boolean isNonPnP = isNonPnP();
		fireSetPropertyEnable(this, "hardwareType", isNonPnP);
		fireSetPropertyEnable(this, "ioport", isNonPnP);
		fireSetPropertyEnable(this, "interrupt", isNonPnP);
	}

	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
		restart();
	}

	public void setFrom(int from) {
		this.from = from;
		restart();
	}

	public void setHardwareType(TPCANType hardwareType) {
		this.hardwareType = hardwareType;
		restart();
	}

	public void setInterrupt(short interrupt) {
		this.interrupt = interrupt;
		restart();
	}

	public void setIoport(int ioport) {
		this.ioport = ioport;
		restart();
	}

	public void setTo(int to) {
		this.to = to;
		restart();
	}
}
