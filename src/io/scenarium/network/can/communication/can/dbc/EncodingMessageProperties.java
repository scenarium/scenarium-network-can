/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.communication.can.dbc;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import io.scenarium.network.can.editors.can.SendMode;

public class EncodingMessageProperties {
	private final int id;
	private final String name;
	private SendMode sendMode;
	private String additionalInfo;
	private ArrayList<EncodingSignalProperties> signalProperties;

	public EncodingMessageProperties(int id, String name) {
		this.id = id;
		this.name = name;
		this.sendMode = SendMode.ONNEWDATA;
		this.signalProperties = new ArrayList<>();
	}

	public EncodingMessageProperties(int id, String name, SendMode sendMode, String additionalInfo, ArrayList<EncodingSignalProperties> signalProperties) {
		this.id = id;
		this.name = name;
		this.sendMode = sendMode;
		this.additionalInfo = additionalInfo == null || additionalInfo.equals("_") ? null : additionalInfo;
		this.signalProperties = signalProperties;
	}

	@Override
	public EncodingMessageProperties clone() {
		return new EncodingMessageProperties(this.id, this.name, this.sendMode, this.additionalInfo,
				this.signalProperties.stream().map(e -> e.clone()).collect(Collectors.toCollection(ArrayList::new)));
	}

	@Override
	public boolean equals(Object emp) {
		return emp instanceof EncodingMessageProperties ? ((EncodingMessageProperties) emp).id == this.id && ((EncodingMessageProperties) emp).name.equals(this.name) : false;
	}

	public String getAdditionalInfo() {
		return this.additionalInfo;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public SendMode getSendMode() {
		return this.sendMode;
	}

	public ArrayList<EncodingSignalProperties> getSignalProperties() {
		return this.signalProperties;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.name);
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setSendMode(SendMode sendMode) {
		if (this.sendMode == sendMode)
			return;
		this.sendMode = sendMode;
		if (sendMode == SendMode.TIMER && this.additionalInfo.isEmpty())
			this.additionalInfo = "100";
	}

	public void setSignalProperties(ArrayList<EncodingSignalProperties> signalProperties) {
		this.signalProperties = signalProperties;
	}

	@Override
	public String toString() {
		return "Id: " + this.id + " Name: " + this.name + " SendMode: " + this.sendMode + " SignalProperties: " + this.signalProperties;
	}

}
