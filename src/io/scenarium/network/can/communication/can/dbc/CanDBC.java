/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.communication.can.dbc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.swing.event.EventListenerList;

import io.scenarium.network.can.communication.can.LoadedListener;
import io.scenarium.network.can.internal.Log;

public class CanDBC {
	private static final String SPLITREGEXP = " (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
	private static final String BO_BALISE = "BO_ ";
	private static final String SG_BALISE = "SG_ ";
	private static final String SIG_VALTYPE_BALISE = "SIG_VALTYPE_ ";
	private static final String VAL_BALISE = "VAL_ ";

	public static void main(String[] args) {
		CanDBC canDBC = new CanDBC();
		canDBC.setCanDBCFile(new File("/home/revilloud/Téléchargements/Fwd TR  Mobileye Extended Protocol documentation + dbc files(1)/ExtLogData2.dbc"));
		DBCMessage m = canDBC.getPropById(1616);
		byte[] data = new byte[m.getSize()];
		m.getSignal("Fixed_Horizon").encode(data, 1.256864f);
		m.getSignal("Fixed_Yaw").encode(data, 16.256864f);
		DBCSignal.showBytesCompact(data);
		Log.info("" + m.getSignal("Fixed_Horizon").decode(data));
		Log.info("" + m.getSignal("Fixed_Yaw").decode(data));
	}

	private File canDBCFile;
	private final EventListenerList listeners = new EventListenerList();

	private HashMap<Integer, DBCMessage> messages;

	public CanDBC() {}

	public CanDBC(File file) {
		setCanDBCFile(file);
	}

	public void addOpenListener(LoadedListener listener) {
		this.listeners.add(LoadedListener.class, listener);
	}

	private void fireLoaded() {
		for (LoadedListener listener : this.listeners.getListeners(LoadedListener.class))
			listener.loaded();
	}

	public File getCanDBCFile() {
		return this.canDBCFile;
	}

	public ArrayList<DBCSignal> getSignals() {
		ArrayList<DBCSignal> signals = new ArrayList<>();
		if (this.messages == null) {
			loadIfNeeded();
			if (this.messages == null)
				return null;
		}
		this.messages.forEach((id, message) -> message.getSignals().forEach(signal -> signals.add(signal)));
		return signals;
	}

	public ArrayList<MessageIdentifier> getMessagesIdentifier() {
		ArrayList<MessageIdentifier> messages = new ArrayList<>();
		loadIfNeeded();
		if (this.messages != null)
			this.messages.forEach((id, message) -> messages.add(toMessagesIdentifier(message)));
		return messages;
	}

	public DBCMessage getPropById(int id) {
		if (this.messages == null)
			return null;
		return this.messages.get(id);
	}

	public ArrayList<SignalIdentifier> getSignalsIdentifier() {
		ArrayList<SignalIdentifier> signals = new ArrayList<>();
		loadIfNeeded();
		if (this.messages != null)
			this.messages.forEach((id, message) -> message.getSignals().forEach(signal -> signals.add(toSignalIdentifier(message, signal))));
		return signals;
	}

	public boolean hasMessage() {
		return this.messages != null && !this.messages.isEmpty();
	}

	public synchronized void loadIfNeeded() {
		if (this.messages == null)
			reload();
	}

	private synchronized void reload() {
		if (this.canDBCFile == null)
			return;
		this.messages = new HashMap<>();
		try {
			try (BufferedReader br = new BufferedReader(new FileReader(this.canDBCFile))) {
				String line = null;
				int nbLine = 0;
				while (line != null || (line = br.readLine()) != null) {
					nbLine++;
					line = line.trim();
					if (line.startsWith(BO_BALISE))
						try {
							String[] boStElements = line.split(SPLITREGEXP);
							int index = 1;
							long id = Long.parseLong(boStElements[index++]);
							String boName = boStElements[index++];
							boName = boName.substring(0, boName.length() - 1);
							int size = Integer.parseInt(boStElements[index++]);
							String sender = boStElements[index++];
							ArrayList<DBCSignal> signals = new ArrayList<>();
							String subLine;
							int nbSubLine = 0;
							while ((subLine = br.readLine()) != null) {
								nbLine++;
								subLine = subLine.trim();
								if (!subLine.startsWith(SG_BALISE)) {
									line = subLine;
									nbLine--;
									break;
								}
								nbSubLine++;
								try {
									String[] subElements = subLine.split(SPLITREGEXP);
									int subIndex = 1;
									String name = subElements[subIndex++];
									if (!subElements[subIndex++].equals(":"))
										throw new IllegalArgumentException("Do not find ':' separator in line: " + subLine);
									String formatInfo = subElements[subIndex++];
									int idFirstSeparator = formatInfo.indexOf("|");
									if (idFirstSeparator == -1)
										throw new IllegalArgumentException("Do not find '|' separator in line: " + subLine);
									byte firstBit = Byte.parseByte(formatInfo.substring(0, idFirstSeparator));
									int idSecondSeparator = formatInfo.indexOf("@");
									if (idSecondSeparator == -1)
										throw new IllegalArgumentException("Do not find '@' separator in line: " + subLine);
									byte msgSize = Byte.parseByte(formatInfo.substring(idFirstSeparator + 1, idSecondSeparator));
									char charCodage = formatInfo.charAt(++idSecondSeparator);
									Boolean isIntel;
									if (charCodage == '1')
										isIntel = true;
									else if (charCodage == '0')
										isIntel = false;
									else
										throw new IllegalArgumentException("The codage: " + charCodage + " is unknown in line: " + subLine);
									char charFormat = formatInfo.charAt(++idSecondSeparator);
									int format = charFormat == '+' ? DBCSignal.UNSIGNED : charFormat == '-' ? DBCSignal.SIGNED : -1;
									if (format == -1)
										throw new IllegalArgumentException("The format: " + charFormat + " is unknown in line: " + subLine);
									String scaleOffsetInfo = subElements[subIndex++];
									if (scaleOffsetInfo.charAt(0) != '(' || scaleOffsetInfo.charAt(scaleOffsetInfo.length() - 1) != ')')
										throw new IllegalArgumentException("The scale and offset properties: " + scaleOffsetInfo + " do not begin with ( and finish with ) in line: " + subLine);
									scaleOffsetInfo = scaleOffsetInfo.substring(1, scaleOffsetInfo.length() - 1);
									int idSeparator = scaleOffsetInfo.indexOf(",");
									if (idSeparator == -1)
										throw new IllegalArgumentException("Do not find ',' separator in line: " + subLine);
									double scale = Double.parseDouble(scaleOffsetInfo.substring(0, idSeparator));
									double offset = Double.parseDouble(scaleOffsetInfo.substring(idSeparator + 1, scaleOffsetInfo.length()));
									String minMaxInfo = subElements[subIndex++];
									if (minMaxInfo.charAt(0) != '[' || minMaxInfo.charAt(minMaxInfo.length() - 1) != ']')
										throw new IllegalArgumentException("The min and max properties: " + minMaxInfo + " do not begin with [ and finish with ] in line: " + subLine);
									minMaxInfo = minMaxInfo.substring(1, minMaxInfo.length() - 1);
									idSeparator = minMaxInfo.indexOf("|");
									if (idSeparator == -1)
										throw new IllegalArgumentException("Do not find '|' separator in line: " + subLine);
									double min = Double.parseDouble(minMaxInfo.substring(0, idSeparator));
									double max = Double.parseDouble(minMaxInfo.substring(idSeparator + 1, minMaxInfo.length()));
									if (min == 0 && max == 0) {
										min = -Double.MAX_VALUE;
										max = Double.MAX_VALUE;
									}
									String unit = subElements[subIndex++];
									if (unit.charAt(0) == '"')
										while (unit.charAt(unit.length() - 1) != '"')
											unit += " " + subElements[subIndex++];
									else
										throw new IllegalArgumentException("The unit properties: " + minMaxInfo + " do not begin with \" and finish with \" in line: " + subLine);
									// if (unit.charAt(0) != '"' || unit.charAt(unit.length() - 1) != '"')
									// throw new IllegalArgumentException("The unit properties: " + minMaxInfo + " do not begin with \" and finish with \" in line: " + subLine);
									signals.add(new DBCSignal(name, size, isIntel, format, firstBit, msgSize, scale, offset, min, max, unit.substring(1, unit.length() - 1), subElements[subIndex++]));
								} catch (Exception e) {
									if (e instanceof IllegalArgumentException)
										Log.error(e.getClass().getSimpleName() + ": " + e.getMessage());
									else
										Log.error("Bad formatted " + SG_BALISE + ": " + nbSubLine + " of block: " + id);
								}
							}
							if (id > Integer.MIN_VALUE || id < Integer.MAX_VALUE)
								this.messages.put((int) id, new DBCMessage((int) id, boName, size, sender, signals));
						} catch (Exception e) {
							e.printStackTrace();
							Log.error("Bad formatted " + BO_BALISE + ": in line: " + line);
						}
					else if (line.startsWith(SIG_VALTYPE_BALISE))
						try {
							while (!line.endsWith(";"))
								line = line + br.readLine();
							line = line.substring(0, line.length() - 1);
							int i = SIG_VALTYPE_BALISE.length();
							if (i != line.length()) {
								String[] elements = line.split(SPLITREGEXP);
								int index = 1;
								long id = Long.parseLong(elements[index++]);
								String sigName = elements[index++];
								if (!elements[index++].equals(":"))
									throw new IllegalArgumentException("Do not find ':' separator in line: " + line);
								int typei = Integer.parseInt(elements[index++]);
								int format = typei == 0 ? DBCSignal.IEEEDOUBLE : typei == 1 ? DBCSignal.IEEEFLOAT : -1;
								if (format == -1)
									new IllegalArgumentException(typei + " is not a valid type in line: " + line);
								DBCMessage message = this.messages.get((int) id);
								DBCSignal signal = message.getSignal(sigName);
								signal.format = format;

							}
						} catch (Exception e) {
							Log.error("Bad formatted " + SIG_VALTYPE_BALISE + ": in line: " + nbLine + "\nCause by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
						}
					else if (line.startsWith(VAL_BALISE))
						try {
							while (!line.endsWith(";"))
								line = line + br.readLine();
							line = line.substring(0, line.length() - 1);
							int i = VAL_BALISE.length();
							if (i != line.length()) {
								String[] elements = line.split(SPLITREGEXP);
								int index = 1;
								DBCSignal signal = this.messages.get((int) Long.parseLong(elements[index++])).getSignal(elements[index++]);
								HashMap<Integer, String> enumMap = new HashMap<>();
								while (index != elements.length) {
									int value = Integer.parseInt(elements[index++]);
									String enumName = elements[index++];
									enumMap.put(value, enumName.substring(1, enumName.length() - 1));
								}
								signal.setEnumMap(enumMap);
							}
						} catch (Exception e) {
							Log.error("Bad formatted " + SIG_VALTYPE_BALISE + ": in line: " + nbLine + "\nCause by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
						}
					line = null;
				}
			}
		} catch (IOException e) {
			if (this.canDBCFile.getAbsolutePath().isBlank())
				Log.error("Error while loading file: " + e.getMessage());
			this.messages = null;
		}
		fireLoaded();
	}

	public void removeOpenListener(LoadedListener listener) {
		this.listeners.remove(LoadedListener.class, listener);
	}

	public void setCanDBCFile(File canDBCFile) {
		this.canDBCFile = canDBCFile;
		reload();
	}

	private static MessageIdentifier toMessagesIdentifier(DBCMessage message) {
		return new MessageIdentifier(message.getId(), message.getName(), message.getSize(),
				message.getSignals().stream().map(e -> toSignalIdentifier(message, e)).collect(Collectors.toCollection(ArrayList::new)));
	}

	private static SignalIdentifier toSignalIdentifier(DBCMessage message, DBCSignal signal) {
		return new SignalIdentifier(message.getId(), message.getName(), signal.name, signal.min, signal.max, signal.unit, signal.getEnumMap());
	}

	@Override
	public String toString() {
		return this.canDBCFile == null ? "No DBC" : this.canDBCFile.toString() + " Nb Messages: " + (this.messages == null ? 0 : this.messages.size());
	}
}
