/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.communication.can;

import java.util.BitSet;

import io.beanmanager.editors.DynamicPossibilities;
import io.beanmanager.editors.basic.BitSetInfo;
import io.scenarium.core.tools.LibraryUtils;
import io.scenarium.network.can.internal.Log;
import io.scenarium.network.can.model.CanTrame;

public class CANVector extends CanProvider {
	private static final String CANVECTORLIB = "CANWrapper";
	private static boolean isAvailable = LibraryUtils.loadLibrariesFromRessources(CANVector.class, e -> "\nDrivers of the Peak's card probably not installed", "CANWrapper");

	public static void main(String[] args) throws Exception {
		if (!isAvailable)
			return;
		CANVector can = new CANVector();
		can.channel = can.getAvailableChannel()[1];
		can.init((canTrame, timeStamp) -> Log.info("At: " + timeStamp + "\tTrame: " + canTrame));
		can.setFilterType(FilterType.EXTENDED);
		for (int i = 0; i < 100000000; i++) {
			can.sendTrame(new CanTrame(new byte[] { 0, 5, 5, 10, 5, 10, 15, 20, 25, 30, 35, 40 }));
			Log.info("trame: " + i);
			Thread.sleep(1);
		}
		Thread.sleep(10000);
		can.death();
	}

	private boolean isRunning = false;
	@DynamicPossibilities(possibleChoicesMethod = "getAvailableCanChannel")
	private String channel;
	private int baudrate = 500000;
	private int rxQueueSize = 4096;
	private FilterType filterType = FilterType.STANDARD;

	@BitSetInfo(maxSize = 32)
	private BitSet rxMask;

	@BitSetInfo(maxSize = 32)
	private BitSet rxCode;

	public native String activateChannel();

	public native String closeDriver();

	@Override
	public void connect() throws CanException {
		if (!isAvailable)
			throw new CanException("Can provider not available");
		String error = openDriver();
		if (error != null)
			throw new CanException("Cannot open the driver, error code: ");
		if (this.channel == null)
			throw new CanException("No channel selected");
		error = initChannel(this.channel, this.baudrate, this.rxQueueSize);
		if (error != null)
			throw new CanException("Cannot init channel: " + this.channel + ", error code: " + error);
		error = createRxThread();
		if (error != null)
			throw new CanException("Cannot create Rx Thread, error code: " + error);
		error = setChannelAcceptance(getDataFromBitSet(this.rxCode, 4), getDataFromBitSet(this.rxMask, 4), this.filterType == FilterType.EXTENDED);
		if (error != null)
			throw new CanException("Cannot set rxCode and rxMask, error code: " + error);
		error = activateChannel();
		if (error != null)
			throw new CanException("Cannot activate the channel, error code: " + error);
	}

	// Not tested
	public static byte[] getDataFromBitSet(BitSet bitSet, int size) {
		if (bitSet == null || bitSet.length() == 0)
			return new byte[size];
		byte[] byteArray = bitSet.toByteArray();
		for (int i = 0; i < byteArray.length / 2; i++) {
			byte temp = byteArray[i];
			byteArray[i] = byteArray[byteArray.length - i - 1];
			byteArray[byteArray.length - i - 1] = temp;
		}
		if (byteArray.length != size) {
			byte[] temp = new byte[size];
			System.arraycopy(byteArray, 0, temp, temp.length - byteArray.length, byteArray.length);
			byteArray = temp;
		}
		return byteArray;
	}

	public native String createRxThread();

	public native String death();

	public String[] getAvailableCanChannel() {
		if (!isAvailable)
			return new String[0];
		String error = openDriver();
		if (error != null) {
			Log.error("Cannot open driver, error code: " + error);
			return new String[0];
		}
		String[] channels = getAvailableChannel();
		error = closeDriver();
		if (error != null)
			Log.error(error);
		return channels;
	}

	@Override
	public native String[] getAvailableChannel();

	public int getBaudrate() {
		return this.baudrate;
	}

	public String getChannel() {
		return this.channel;
	}

	public FilterType getFilterType() {
		return this.filterType;
	}

	public BitSet getRxCode() {
		return this.rxCode;
	}

	public BitSet getRxMask() {
		return this.rxMask;
	}

	public int getRxQueueSize() {
		return this.rxQueueSize;
	}

	public native String initChannel(String channelName, int baudrate, int rxQueueSize);

	// private int notifyEveryXFrames = 1;
	// private int resetDelayOnBussOff = 10000;
	public native String openDriver();

	public void receive(byte[] data) {
		this.receiver.receive(new CanTrame(data), System.currentTimeMillis());
	}

	@Override
	public void release() {
		String error = death();
		if (error != null)
			Log.error("Cannot close the driver, error code: " + error);
		this.isRunning = false;
	}

	@Override
	public void sendTrame(CanTrame canTrame) {
		byte[] trame = canTrame.getTrame().clone();
		if (this.filterType == FilterType.STANDARD && canTrame.isExtendedFrame()) {
			Log.error("cannot send trame: " + canTrame + "\nId larger than 11 bits specified in standard can 2.0A");
			return;
		}
		if (this.filterType == FilterType.EXTENDED)
			trame[0] |= 0b10000000;
		String error = transmit(trame);
		if (error != null)
			Log.error("Cannot transmit canTrame: " + canTrame + ", error code: " + error);
		return;
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
		restart();
	}

	public void setChannel(String channel) {
		this.channel = channel;
		restart();
	}

	public native String setChannelAcceptance(byte[] codes, byte[] mask, boolean isExtended);

	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
		restart();
	}

	public void setRxCode(BitSet rxCode) {
		this.rxCode = rxCode;
		restart();
	}
	// public int getNotifyEveryXFrames() {
	// return notifyEveryXFrames;
	// }
	//
	// public void setNotifyEveryXFrames(int notifyEveryXFrames) {
	// this.notifyEveryXFrames = notifyEveryXFrames;
	// }
	//
	// public int getResetDelayOnBussOff() {
	// return resetDelayOnBussOff;
	// }
	//
	// public void setResetDelayOnBussOff(int resetDelayOnBussOff) {
	// this.resetDelayOnBussOff = resetDelayOnBussOff;
	// }

	public void setRxMask(BitSet rxMask) {
		this.rxMask = rxMask;
		restart();
	}

	public void setRxQueueSize(int rxQueueSize) {
		this.rxQueueSize = rxQueueSize;
		restart();
	}

	public native String transmit(byte[] datas);
}
