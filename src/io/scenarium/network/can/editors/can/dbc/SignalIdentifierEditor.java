/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.editors.can.dbc;

import io.beanmanager.editors.PropertyEditor;
import io.scenarium.network.can.communication.can.dbc.SignalIdentifier;

public class SignalIdentifierEditor extends PropertyEditor<SignalIdentifier> {

	@Override
	public String getAsText() {
		SignalIdentifier cvi = getValue();
		return cvi.getId() + " " + cvi.getMessageName() + " " + cvi.getName();
	}

	@Override
	public void setAsText(String text) {
		int i1 = text.indexOf(" ");
		int i2 = text.lastIndexOf(" ");
		setValue(new SignalIdentifier(Integer.parseInt(text.substring(0, i1)), text.substring(i1 + 1, i2), text.substring(i2 + 1), -1, -1, null, null));
	}
}
