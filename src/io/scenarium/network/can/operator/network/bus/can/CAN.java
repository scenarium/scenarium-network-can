/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.can.operator.network.bus.can;

import java.util.ArrayList;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.scenarium.flow.BlockInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;
import io.scenarium.flow.operator.DeclaredOutputChangeListener;
import io.scenarium.network.can.communication.can.CANPeak;
import io.scenarium.network.can.communication.can.CANReceiver;
import io.scenarium.network.can.communication.can.CANVector;
import io.scenarium.network.can.communication.can.CanException;
import io.scenarium.network.can.communication.can.CanProvider;
import io.scenarium.network.can.communication.can.dbc.DBCSignal;
import io.scenarium.network.can.internal.Log;
import io.scenarium.network.can.model.CanTrame;
import io.scenarium.network.can.operator.network.decoder.CanDBCDecoder;

@BlockInfo(info = "Block for the communication with an CAN bus")
public class CAN extends EvolvedOperator implements CANReceiver, DeclaredOutputChangeListener, BeanRenameListener {
	@PropertyInfo(index = 0)
	@BeanInfo(possibleSubclasses = { CANVector.class, CANPeak.class }, alwaysExtend = true)
	private CanProvider canProvider;
	@PropertyInfo(index = 1)
	private boolean generatingCanRawOutput = false;
	@PropertyInfo(index = 2)
	@BeanInfo(alwaysExtend = true)
	private CanDBCDecoder canDBCDecoder;
	private boolean isRunning = false;
	private boolean isAvailable = false;
	private String[] decoderOutputNames;
	private Class<?>[] decoderOutputTypes;

	@Override
	public void birth() throws Exception {
		if (this.canProvider == null)
			return;
		this.isRunning = true;
		onStart(() -> {
			try {
				this.canProvider.init(this);
				this.isAvailable = true;
			} catch (CanException e) {
				Log.error(getBlockName() + ": " + e.getMessage());
				setError(e.getMessage());
			}
		});
		if (this.canDBCDecoder != null)
			this.canDBCDecoder.birth();
	}

	@Override
	public void death() throws Exception {
		if (this.canProvider != null)
			this.canProvider.close();
		if (this.canDBCDecoder != null)
			this.canDBCDecoder.death();
		this.isRunning = false;
	}

	@Override
	public void declaredOutputChanged(String[] names, Class<?>[] types) {
		this.decoderOutputNames = names;
		this.decoderOutputTypes = types;
		updateIOStructure();
	}

	public CanDBCDecoder getCanDBCDecoder() {
		return this.canDBCDecoder;
	}

	public CanProvider getCanProvider() {
		return this.canProvider;
	}

	@Override
	protected void onBlockBound() throws Exception {
		addBlockNameChangeListener(this);
	}

	@Override
	protected void onBlockUnbound() throws Exception {
		removeBlockNameChangeListener(this);
	}

	@Override
	public void updateIOStructure() {
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		if (this.generatingCanRawOutput) {
			names.add(getBlockName());
			types.add(CanTrame.class);
		}
		if (this.decoderOutputNames != null)
			for (int i = 0; i < this.decoderOutputNames.length; i++) {
				names.add(this.decoderOutputNames[i]);
				types.add(this.decoderOutputTypes[i]);
			}
		updateOutputs(names.toArray(String[]::new), types.toArray(Class[]::new));
	}

	public boolean isGeneratingCanRawOutput() {
		return this.generatingCanRawOutput;
	}

	@ParamInfo(in = "in", out = "out")
	public void process(CanTrame... canTrames) throws Exception {
		if (this.canProvider != null && this.isAvailable)
			for (CanTrame canTrame : canTrames)
				this.canProvider.sendTrame(canTrame);
	}

	@Override
	public void receive(CanTrame canTrame, long timestamp) {
		Object[] outputs = generateOuputsVector();
		boolean isEmpty = this.generatingCanRawOutput;
		if (this.generatingCanRawOutput) {
			int idOfOut = getOutputIndex(getBlockName());
			outputs[idOfOut] = canTrame;
			isEmpty = false;
		}
		if (this.canDBCDecoder != null) {
			ArrayList<DBCSignal> datasProp = this.canDBCDecoder.getDatasProp(canTrame);
			if (datasProp != null)
				for (DBCSignal canDBCProp : datasProp) {
					outputs[getOutputIndex(canTrame.getId() + "_" + canDBCProp.name)] = this.canDBCDecoder.isEnumAsString() && canDBCProp.isEnum() ? canDBCProp.decodeAsString(canTrame.getData())
							: canDBCProp.decode(canTrame.getData());
					isEmpty = false;
				}
		}
		if (!isEmpty)
			triggerOutput(outputs, timestamp);
	}

	public void setCanDBCDecoder(CanDBCDecoder canDBCDecoder) {
		if (this.canDBCDecoder != null) {
			this.canDBCDecoder.removeDeclaredOutputChangeListener(this);
			this.canDBCDecoder.setTrigger(null);
			declaredOutputChanged(null, null);
			if (this.isRunning)
				this.canDBCDecoder.death();
		}
		this.canDBCDecoder = canDBCDecoder;
		if (this.canDBCDecoder != null) {
			this.canDBCDecoder.addDeclaredOutputChangeListener(this);
			this.canDBCDecoder.updateIOStructure();
			if (this.isRunning)
				this.canDBCDecoder.birth();
		}
	}

	public void setCanProvider(CanProvider canProvider) {
		if (this.isRunning) {
			this.isAvailable = false;
			CanProvider cp = this.canProvider;
			this.canProvider = null;
			cp.close();
			this.canProvider = canProvider;
			try {
				birth();
				this.isAvailable = true;
			} catch (Exception e) {
				Log.error(e.getMessage());
			}
		} else
			this.canProvider = canProvider;
	}

	public void setGeneratingCanRawOutput(boolean generatingCanRawOutput) {
		this.generatingCanRawOutput = generatingCanRawOutput;
		updateIOStructure();
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		updateIOStructure();
	}
}
