import io.scenarium.network.can.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.network.can {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;
	requires transitive CANPeak2016;
	requires javafx.controls;

	exports io.scenarium.network.can;
	exports io.scenarium.network.can.model;
	exports io.scenarium.network.can.communication.can;
	exports io.scenarium.network.can.communication.can.dbc;
	exports io.scenarium.network.can.editors.can;
	exports io.scenarium.network.can.editors.can.dbc;
	exports io.scenarium.network.can.operator.network.bus.can;
	exports io.scenarium.network.can.operator.network.decoder;
	exports io.scenarium.network.can.operator.network.encoder;
}