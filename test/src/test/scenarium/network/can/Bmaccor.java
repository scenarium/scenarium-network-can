/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.network.can;

import java.io.File;
import java.util.HashSet;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.ihmtest.FxTest;
import io.beanmanager.struct.Selection;
import io.scenarium.network.can.communication.can.dbc.CanDBC;
import io.scenarium.network.can.communication.can.dbc.EncodingMessageProperties;
import io.scenarium.network.can.communication.can.dbc.MessageIdentifier;
import io.scenarium.network.can.editors.can.dbc.EncodingMessagePropertiesEditor;
import io.scenarium.network.can.operator.network.encoder.CanDBCEncoder;

import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class Bmaccor {

	private Bmaccor() {}

	public static void main(String[] args) {
		PropertyEditorManager.registerEditor(EncodingMessageProperties[].class, EncodingMessagePropertiesEditor.class);

		CanDBCEncoder bean = new CanDBCEncoder();
		bean.setCanDBC(new CanDBC(new File("/home/revilloud/Téléchargements/Fwd TR  Mobileye Extended Protocol documentation + dbc files(1)/ExtLogData2.dbc")));

		int i = 0;
		HashSet<MessageIdentifier> h = new HashSet<>();
		for (MessageIdentifier mi : bean.getCanDBC().getMessagesIdentifier()) {
			if (i++ == 10)
				break;
			h.add(mi);
		}

		bean.setFilters(new Selection<>(h, MessageIdentifier.class));
		final BeanManager beanGui = new BeanManager(bean, "");
		BeanEditor.registerBean(bean, "");
		new FxTest();
		// beanGui.load();
		FxTest.launchIHM(args, s -> {
			GridPane editor = beanGui.getEditor();
			ScrollPane sp = new ScrollPane(editor);

			beanGui.addSizeChangeListener(() -> {
				s.sizeToScene();
			});
			return new VBox(sp, new Button("down"));
		}, false);
	}
}
